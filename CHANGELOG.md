# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.4] - 2018-06-21
### Fixed
- Default value for constructor's $matter argument

## [1.0.3] - 2018-02-13
### Fixed
- Updated code to use DotArray::getArrayCopy() instead of DotArray::export() ^^

## [1.0.2] - 2018-02-13
### Changed
- Use dot-array ^2.0

## [1.0.1] - 2018-01-31
### Changed
- dot-array version

## [1.0.0] - 2018-01-30
- First release
