# Contributing

## Coding standards

The source code of this package follows [PSR-1](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md) and [PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md) coding guidelines.

Please make sure your contribution follows these guidelines: a  `phpcs.xml` configuration file for [phpcs](https://packagist.org/packages/squizlabs/php_codesniffer). Simply run `phpcs` on the command line.

## Tests

All contribution MUST come with tests for [phpunit](https://packagist.org/packages/phpunit/phpunit).

Code coverage SHOUDL be 100%.
