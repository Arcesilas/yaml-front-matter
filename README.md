[![Packagist](https://img.shields.io/packagist/v/arcesilas/yaml-front-matter.svg?style=flat-square)]()
[![PHP from Packagist](https://img.shields.io/packagist/php-v/arcesilas/yaml-front-matter.svg?style=flat-square)]()
[![license](https://img.shields.io/github/license/Arcesilas/yaml-front-matter.svg?style=flat-square)]()
[![Scrutinizer](https://img.shields.io/scrutinizer/g/Arcesilas/yaml-front-matter.svg?style=flat-square)]()
[![Scrutinizer Coverage](https://img.shields.io/scrutinizer/coverage/g/Arcesilas/yaml-front-matter.svg?style=flat-square)]()
[![Scrutinizer Build](https://img.shields.io/scrutinizer/build/g/Arcesilas/yaml-front-matter.svg?style=flat-square)]()

# Yaml Front Matter

This package is especially inspired by [spatie/yaml-front-matter](https://packagist.org/packages/spatie/yaml-front-matter).

Example:

```markdown
---
meta:
    title: 'Hello world'
    type: blog
---

# Hello world!
```

The front matter is delimited with `---` and stored in an array (actually, an instance of `ArrayAccess`):
```php
[
    'meta' => [
        'title' => 'Hello world',
        'type' => blog
    ]
]
```

## Installation

Install with Composer:

```shell
composer require arcesilas/yaml-front-matter
```

## Usage

File: `path/fo/file.md`
```markdown
---
meta:
    title: 'Hello world'
    type: blog
---

# Hello world!
```

```php
<?php

use Arcesilas\YamlFrontMatter\Document;

$document = Document::fromFile('/path/to/file.md');
$document->matter(); // ['meta' => ['title' => 'Hello world','type' => blog]]
$document->matter('meta'); // ['title' => 'Hello world','type' => blog]
$document->matter('meta.title'); // 'Hello world'
$document->body(); // '# Hello world'
```
