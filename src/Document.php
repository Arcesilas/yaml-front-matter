<?php

namespace Arcesilas\YamlFrontMatter;

use Symfony\Component\Yaml\Yaml;
use Arcesilas\DotArray\DotArray;

class Document
{
    /**
     * The document front matter
     * @var DotArray
     */
    protected $matter;

    /**
     * The document body
     * @var string
     */
    protected $body = '';

    /**
     * Create a document from a string
     * @param  string     $contents
     * @return Document
     */
    public static function fromString(string $contents): Document
    {
        // Pattern can be seen as: ($1)?(.*)
        // Which will match any string
        preg_match('`(?:\n?---\n(.*)\n---)?\n?(.*)`s', $contents, $matches);

        // Yaml parser returns NULL with empty string as input
        $matter = Yaml::parse($matches[1]) ?? [];
        return new static($matter, $matches[2]);
    }

    /**
     * Create a document from a file path
     * @param  string   $path
     * @return Document
     */
    public static function fromFile(string $path): Document
    {
        return static::fromString(file_get_contents($path));
    }

    /**
     * @param  array  $matter The document front matter
     * @param  string $body The document body
     */
    public function __construct(array $matter = [], string $body = '')
    {
        $this->matter = new DotArray($matter);
        $this->body = $body;
    }

    /**
     * Returns the document body
     * @param  bool    $trim
     * @return string
     */
    public function body($trim = true): string
    {
        return $trim ? trim($this->body) : $this->body;
    }

    /**
     * Returns the full front matter or a specific key
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    public function matter(string $key = null, $default = null)
    {
        if (null === $key) {
            return $this->matter->getArrayCopy();
        }

        return $this->matter->get($key, $default);
    }

    /**
     * Dump the document
     * @return string
     */
    public function dump(): string
    {
        $return = sprintf(
            "---\n%s\n---\n\n%s",
            trim(Yaml::dump($this->matter->getArrayCopy())),
            $this->body
        );
        return preg_match('`[\n\r]$`', $return) ? $return : $return.PHP_EOL;
    }
}
