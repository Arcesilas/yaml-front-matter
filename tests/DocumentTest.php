<?php

namespace Arcesilas\YamlFrontMatter\Tests;

use PHPUnit\Framework\TestCase;
use Arcesilas\YamlFrontMatter\Document;

class DocumentTest extends TestCase
{

    public function contentsProvider()
    {
        return [
            // Valid matter
            [
                "---\nfoo: bar\n---\nHello world!",
                ['foo' => 'bar'],
                'Equals',
                ['Hello world!']
            ],
            // Invalid matter delimiter
            [
                "---\nfoo: bar\n--\nHello world!",
                [],
                'Contains',
                [
                    'foo: bar',
                    'Hello world!'
                ]
            ],
            // Empty front matter
            [
                "Hello world!",
                [],
                'Equals',
                ['Hello world!']
            ],
            // Empty body
            [
                "---\nfoo: bar\n---",
                ['foo' => 'bar'],
                'Equals',
                ['']
            ]
        ];
    }

    /**
     * @dataProvider contentsProvider
     */
    public function testCreatesDocumentFromString($contents, $expectedMatter, $assertion, $expectedBody)
    {
        $assertion = 'assert'.$assertion;

        $document = Document::fromString($contents);

        $this->assertInstanceOf(Document::class, $document);
        $this->assertEquals($expectedMatter, $document->matter());
        foreach ($expectedBody as $e) {
            $this->$assertion($e, $document->body());
        }
    }

    public function sampleProvider()
    {
        return [[
            [
                'foo' => 'bar',
                'meta' => [
                    'title' => 'Hello world',
                    'type' => 'blog'
                ]
            ],
            'Hello world!'
        ]];
    }

    public function testCreatesDocumentFromFile()
    {
        $document = Document::fromFile(__DIR__.'/document.md');

        $this->assertInstanceOf(Document::class, $document);
        $this->assertEquals('Hello world', $document->matter('meta.title'));
    }

    /**
     * @dataProvider sampleProvider
     */
    public function testAccessNestedMatter($matter, $body)
    {
        $document = new Document($matter, $body);
        $this->assertEquals('Hello world', $document->matter('meta.title'));
        $this->assertEquals('blog', $document->matter('meta.type'));
    }

    public function defaultValuesProvider()
    {
        return [
            [1337, 1337],
            [function () {
                return 42;
            }, 42]
        ];
    }

    /**
     * @dataProvider defaultValuesProvider
     */
    public function testReturnsDefaultValue($defaultValue, $expected)
    {
        $data = $this->sampleProvider();
        list($matter, $body) = $data[0];
        $document = new Document($matter, $body);
        $this->assertEquals($expected, $document->matter('undefined.key', $defaultValue));
    }

    public function testDump()
    {
        $document = new Document([
            'meta' => [
                'title' => 'Hello world',
                'type' => 'blog'
            ]
        ], '# Hello world!');

        $expected = file_get_contents(__DIR__.'/document.md');

        $this->assertEquals($expected, $document->dump());
    }

    public function testEmptyDocumentFromConstructor()
    {
        $document = new Document();
        $this->assertSame([], $document->matter());
    }
}
